# @atlaskit/atlassian-switcher-test-utils

## 0.1.0

### Minor Changes

- [minor][54588e51df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/54588e51df):

  Add recommendationsFeatureFlags to generic-switcher

## 0.0.1

### Patch Changes

- [patch][6bc87c7501](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/6bc87c7501):

  Split mockEndpoints into a separate package
